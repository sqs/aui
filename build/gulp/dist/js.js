'use strict';

var commander = require('../../lib/commander');
var fs = require('fs-extra');
var galvatron = require('../../lib/galvatron');
var gulp = require('gulp');
var gulpRename = require('gulp-rename');
var minify = require('../../lib/minify');

module.exports = function () {
    var bundle = galvatron.bundle('src/js/aui*.js', {
        common: '**/aui.js'
    });

    fs.removeSync('dist/aui/js');
    fs.mkdirsSync('dist/aui/js');

    return gulp.src(bundle.files)
        .pipe(bundle.watchIf(commander.watch))
        .pipe(bundle.stream())
        .pipe(gulp.dest('dist/aui/js'))
        .pipe(minify())
        .pipe(gulpRename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/aui/js'));
};
