'use strict';

var commander = require('../../lib/commander');
var fs = require('fs-extra');
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var less = require('../../lib/less');
var mac = require('../../lib/mac');
var streamIf = require('../../lib/stream-if');

function cleanup () {
    fs.removeSync('lib/css');
    fs.mkdirsSync('lib/css');
}

function mainImages () {
    return gulp.src('src/less/images/**')
        .pipe(gulp.dest('lib/css/images'));
}

function fonts () {
    return gulp.src('src/less/fonts/**')
        .pipe(gulp.dest('lib/css/fonts'));
}

function select2Images () {
    return gulp.src([
        'src/css-vendor/jquery/plugins/*.png',
        'src/css-vendor/jquery/plugins/*.gif'
    ]).pipe(gulp.dest('lib/css'));
}

function lessFiles () {
    var files = 'src/less/**.less';

    return gulp.src(files)
        .pipe(sourcemaps.init())
            .pipe(less(files))
            .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('lib/css'));
}

module.exports = mac.series(
    cleanup,
    mac.parallel(
        mainImages,
        fonts,
        select2Images,
        lessFiles
    )
);
