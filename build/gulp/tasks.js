'use strict';

var glob = require('glob');
var log = require('../lib/log');
var path = require('path');

var buildPath = 'build/gulp';

module.exports = function () {
    log.info('Available Tasks');
    log.info('---------------');
    log.info();
    log.info('To see specific information about a task call the task with `--help`.');

    glob.sync(path.join(buildPath, '{*,**/*}.js')).forEach(function (file) {
        var name = file.replace(buildPath + '/', '').replace('.js', '');
        log.info('  ' + name);
    });
};
