var path = require('path');
var less = require('gulp-less');
var gulpWatchLess = require('gulp-watch-less');

var streamIf = require('./stream-if');
var commander = require('./commander');

var options = {
    paths: [ path.join(__dirname, '..', '..', 'bower_components') ]
}

module.exports = function (toWatch) {
    return streamIf(commander.watch && gulpWatchLess(toWatch, options))
            .pipe(less(options));
};
