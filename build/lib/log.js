'use strict';

var commander = require('commander');
var debug = require('./debug');

commander
    .option('-d, --debug', 'Enable logging.')
    .parse(process.argv);

var logger = debug(commander.debug || 'info,warn,error');

module.exports = {
    info: logger('info'),
    warn: logger('warn'),
    error: logger('error')
};
