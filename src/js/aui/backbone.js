import _ from './underscore';
import Backbone from '../../js-vendor/backbone/backbone';

if (!window.Backbone) {
    window.Backbone = Backbone;
}

export default window.Backbone;
