'use strict';

import createElement from '../create-element';

var NAMESPACE = 'AJS';

export default function (name, value) {
    if (!window[NAMESPACE]) {
        window[NAMESPACE] = createElement;
    }

    return window[NAMESPACE][name] = value;
}
