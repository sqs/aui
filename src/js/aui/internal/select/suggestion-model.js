import Backbone from '../../backbone';

export default Backbone.Model.extend({
    idAttribute: 'label',
    getLabel: function () {
        return this.get('label') || this.get('value');
    }
});