'use strict';

import { fn as deprecateFn } from './internal/deprecation';
import globalize from './internal/globalize';

/**
 * Prevent further handling of an event. Returns false, which you should use as the return value of your event handler:
 * return stopEvent(e);
 *
 * @param {jQuery.Event} e jQuery event
 *
 * @returns {Boolean}
 */
function stopEvent (e) {
    e.stopPropagation();
    return false; // required for JWebUnit pop-up links to work properly
}

var stopEvent = deprecateFn(stopEvent, 'stopEvent', {
    alternativeName: 'preventDefault()',
    sinceVersion: '5.8.0'
});

globalize('stopEvent', stopEvent);

export default stopEvent;
