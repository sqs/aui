'use strict';

import $ from 'jquery';
import enforce from './internal/enforcer';
import skateTemplateHtml from 'skatejs-template-html';
import skate from 'skatejs';

function getInput (element) {
    return element.querySelector('input');
}

function setBooleanAttribute(element, attribute, value) {
    value ? element.setAttribute(attribute, '') : element.removeAttribute(attribute);
    return !!value;
}

function getAttributeHandler (attributeName) {
    return {
        removed: function (element) {
            getInput(element).removeAttribute(attributeName);
        },
        fallback: function (element, change) {
            getInput(element).setAttribute(attributeName, change.newValue + (attributeName === 'id' ? '-input' : ''));
        }
    };
}

var labelHandler = {
    removed: function (element) {
        getInput(element).removeAttribute('aria-label');
    },
    fallback: function (element, change) {
        getInput(element).setAttribute('aria-label', change.newValue);
    }
};

function getTooltipContent() {
    /* jshint validthis: true */
    return this._input.checked ? this.tooltipOn : this.tooltipOff;
}

function clickHandler(element, e) {
    if (!element.disabled && !element.busy && e.target !== element._input) {
        element._input.click();
    }
}

/**
 * Workaround to prevent pressing SPACE on busy state.
 * Preventing click event still makes the toggle flip and revert back.
 * So on CSS side, the input has "pointer-events: none" on busy state.
 */
function bindEventsToInput(element) {
    element._input.addEventListener('keydown', function (e) {
        if (element.busy && e.keyCode ===  AJS.keyCode.SPACE) {
            e.preventDefault();
        }
    });
    // prevent toggle can be trigger through SPACE key on Firefox
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        element._input.addEventListener('click', function (e) {
            if (element.busy) {
                e.preventDefault();
            }
        });
    }
}

skate('aui-toggle', {
    template: skateTemplateHtml(
        '<input type="checkbox" class="aui-toggle-input">',
        '<span class="aui-toggle-view">',
            '<span class="aui-toggle-tick aui-icon aui-icon-small aui-iconfont-success"></span>',
            '<span class="aui-toggle-cross aui-icon aui-icon-small aui-iconfont-close-dialog"></span>',
        '</span>'
    ),
    created: function (element) {
        element._input = getInput(element); // avoid using _input in attribute handlers
        element._tick = element.querySelector('.aui-toggle-tick');
        element._cross = element.querySelector('.aui-toggle-cross');

        $(element).tooltip({title: getTooltipContent, gravity: 's', hoverable: false});
        bindEventsToInput(element);
    },
    attached: function (element) {
        enforce(element).attributeExists('label');
    },
    events: {
        click: clickHandler
    },
    attributes: {
        id: getAttributeHandler('id'),
        checked: getAttributeHandler('checked'),
        disabled: getAttributeHandler('disabled'),
        form: getAttributeHandler('form'),
        name: getAttributeHandler('name'),
        value: getAttributeHandler('value'),
        'tooltip-on': {value: AJS.I18n.getText('aui.toggle.on')},
        'tooltip-off': {value: AJS.I18n.getText('aui.toggle.off')},
        label: labelHandler
    },
    prototype: {
        focus: function () {
            this._input.focus();
            return this;
        },
        get checked () {
            return this._input.checked;
        },
        set checked (value) {
            this._input.checked = value;    // need to explicitly set the property here because the checkbox's property doesn't change with it's attribute after it is clicked
            return setBooleanAttribute(this, 'checked', value);
        },
        get disabled () {
            return this._input.disabled;
        },
        set disabled (value) {
            return setBooleanAttribute(this, 'disabled', value);
        },
        get form () {
            return this._input.form;
        },
        set form (value) {  // This setter does nothing according to the HTML5 spec.
            return this._input.form = value;
        },
        get name () {
            return this._input.name;
        },
        set name (value) {
            this.setAttribute('name', value);
            return value;
        },
        get value () {
            return this._input.value;
        },
        set value (value) {  // Setting the value of an input to null sets it to empty string.
            this.setAttribute('value', value === null ? '' : value);
            return value;
        },
        get busy () {
            return this.hasAttribute('aria-busy');
        },
        set busy (value) {
            if (value) {
                this.setAttribute('aria-busy', '');
                if (this.checked) {
                    $(this._tick).spin({zIndex: null});
                } else {
                    $(this._cross).spin({zIndex: null, color: 'black'});
                }
            } else {
                this.removeAttribute('aria-busy');
                $(this._cross).spinStop();
                $(this._tick).spinStop();
            }
            return value;
        }
    }
});
