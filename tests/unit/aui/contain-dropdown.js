'use strict';

import containDropdown from '../../../src/js/aui/contain-dropdown';

describe('aui/contain-dropdown', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            containDropdown: containDropdown
        });
    });
});
